function xhr(method, url, data, done, failed) {
  var xhr = new XMLHttpRequest();
  xhr.open(method, url);
  xhr.onload = function() {
    done.call(xhr);
  }
  xhr.onerror = function() {
    failed.call(xhr)
  }
  xhr.send(data);
}

function getAnthemVideo() {
  return document.getElementById("anthem-video");
}

function getLyricsContainer() {
  return document.getElementById("lyrics");
}

function lowerInitialVolume() {
  getAnthemVideo().volume=0.2;
}

function removeAllChildElements(element) {
  while(element.firstChild) {
    element.removeChild(element.firstChild);
  }
}

var trackTimers = [];

function onLyricActive(lyricId) {
  // Find the word, highlight it
  var active = document.getElementById("lyric-" + lyricId);
  Array.prototype.slice.apply(document.getElementsByClassName("lyric")).forEach(function(lyric) {
    lyric.className = "lyric";
  });
  active.className = "lyric lyric-active";
}

function onCueChangeEvent(cueChangeEvent) {
  if(cueChangeEvent.currentTarget.activeCues.length === 0) {
    removeAllChildElements(getLyricsContainer());
    return;
  }

  if(!getAnthemVideo().paused) {
    onCueChange(cueChangeEvent.currentTarget.activeCues[0]);
  }
}

function onCueChange(cueChange) {
  // Display the current cue
  var activeCue = JSON.parse(cueChange.text);
  var currentTime = getAnthemVideo().currentTime;
  var startTime = cueChange.startTime;

  var offset = currentTime - startTime;

  // Render the cue
  var words = activeCue.words;

  // Clear the lyrics
  removeAllChildElements(getLyricsContainer());

  words.forEach(function(word, index) {
    var newSpan = document.createElement('span');
    newSpan.innerText = word.word + " ";
    newSpan.id = "lyric-" + index;
    newSpan.className = "lyric";
    getLyricsContainer().appendChild(newSpan);
  });

  trackTimers.forEach(clearTimeout);

  // Register timeouts for when each word will be active
  activeCue.words.forEach(function(cueWord, index) {
    var cueTime = ~~(cueWord.offset*1000 - offset*1000);
    if(cueTime < 0) {
      cueTime = 0;
    }

    trackTimers.push(
      setTimeout(onLyricActive.bind(this, index), cueTime)
    );
  });
}

function getCurrentCue(video) {
  return video.textTracks[0].activeCues[0];
}

function onSeek(event) {
  var currentCue = getCurrentCue(getAnthemVideo());

  if(currentCue) {
    onCueChange(currentCue);
  }
}

function onPause(event) {
  trackTimers.forEach(clearTimeout);
}

function onPlay(event) {
  var currentCue = getCurrentCue(getAnthemVideo());

  if(currentCue) {
    onCueChange(currentCue);
  } else {
    removeAllChildElements(getLyricsContainer());
  }
}

function registerHooks() {
  var anthemVideo = getAnthemVideo();

  // Handle a cuechange
  anthemVideo.textTracks[0].oncuechange = onCueChangeEvent;

  // Handle video pause, play and seek events
  anthemVideo.onseek = onSeek;
  anthemVideo.onpause = onPause;
  anthemVideo.onplay = onPlay;
}

window.addEventListener("load",function(eventData) {
  console.log("Ready!");

  // Hide tracks
  Array.prototype.slice.call(document.getElementsByTagName("track")).forEach(function(track) {
    track.mode = "hidden";
  });

  lowerInitialVolume();
  registerHooks();
});
